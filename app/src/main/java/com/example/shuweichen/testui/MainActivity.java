package com.example.shuweichen.testui;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by shuwei.chen
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private RelativeLayout activityMain;
    private LinearLayout layoutBottom;
    private EditText edtInput;
    private Button btnSend;
    private ListView listviewChat;
    private SuperSwipeRefreshLayout swipeRefresh;

    private Context mContext;
    private List<ChatMessage> mChatMessageList;
    private ChatAdapter mChatAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = MainActivity.this;
        setContentView(R.layout.activity_main);
        initView();
        initListener();
        initData();
    }

    /**
     * init UI
     */
    private void initView() {
        activityMain = (RelativeLayout) findViewById(R.id.activity_main);
        layoutBottom = (LinearLayout) findViewById(R.id.layout_bottom);
        edtInput = (EditText) findViewById(R.id.edt_input);
        btnSend = (Button) findViewById(R.id.btn_send);
        listviewChat = (ListView) findViewById(R.id.listview_chat);
        swipeRefresh = (SuperSwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        initSwipeRefresh();
    }

    private void initSwipeRefresh() {
        swipeRefresh = (SuperSwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeRefresh.setOnPullRefreshListener(new SuperSwipeRefreshLayout.OnPullRefreshListener() {

            @Override
            public void onRefresh() {
                //TODO 开始刷新
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                        Toast.makeText(mContext, "nothing to refresh~!", Toast.LENGTH_SHORT).show();
                    }
                }, 2 * 1000);
            }

            @Override
            public void onPullDistance(int distance) {
                //TODO 下拉距离
            }

            @Override
            public void onPullEnable(boolean enable) {
                //TODO 下拉过程中，下拉的距离是否足够出发刷新
            }
        });
    }

    /**
     * init listener
     */
    private void initListener() {
        btnSend.setOnClickListener(this);
    }

    /**
     * init data for display
     */
    private void initData() {
        mChatMessageList = new ArrayList<>();
        mChatAdapter = new ChatAdapter(mContext, mChatMessageList);
        listviewChat.setAdapter(mChatAdapter);
        ChatMessage msg = new ChatMessage();
        msg.setId(1);
        msg.setMe(false);
        msg.setMessage("Hi");
        msg.setDate(DateFormat.getDateTimeInstance().format(new Date()));
        mChatMessageList.add(msg);
        ChatMessage msg1 = new ChatMessage();
        msg1.setId(mChatMessageList.size() + 1);
        msg1.setMe(false);
        msg1.setMessage("How r u doing???");
        msg1.setDate(DateFormat.getDateTimeInstance().format(new Date()));
        mChatMessageList.add(msg1);
    }

    /**
     * Send Msg
     *
     * @param text
     */
    private void sendMsg(String text) {
        ChatMessage msgSend = new ChatMessage();
        msgSend.setId(mChatMessageList.size() + 1);
        msgSend.setMessage(text);
        msgSend.setMe(true);
        msgSend.setDate(DateFormat.getDateTimeInstance().format(new Date()));
        mChatMessageList.add(msgSend);
        mChatAdapter.notifyDataSetChanged();
        listviewChat.setSelection(mChatMessageList.size() - 1);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_send:
                //check if text is null first,then Send Msg
                if (edtInput.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, "text cant be null", Toast.LENGTH_SHORT).show();
                    return;
                }
                sendMsg(edtInput.getText().toString());
                edtInput.setText("");
                break;
        }
    }
}
