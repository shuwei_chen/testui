package com.example.shuweichen.testui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


/**
 * Created by shuwei.chen on 3/3/2017.
 */

public class ChatAdapter extends ArrayAdapter<ChatMessage> {

    private List<ChatMessage> data;
    private Context mContext;
    private LayoutInflater mLayoutInflater;


    public ChatAdapter(@NonNull Context context, @NonNull List<ChatMessage> objects) {
        super(context, 0, objects);
        data = objects;
        mContext = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        ChatMessage chatMessage = getItem(position);
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.item_chat, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        switchLayout(chatMessage.getIsme(), holder, chatMessage);


        return convertView;
    }

    /**
     * 判断左右布局
     *
     * @param isMe
     * @param holder
     * @param msg
     */
    private void switchLayout(boolean isMe, ViewHolder holder, ChatMessage msg) {
        if (isMe) {
            holder.getLeftcontent().setVisibility(View.VISIBLE);
            holder.getLeftcontent().setText(msg.getMessage());
            holder.getLeftime().setVisibility(View.VISIBLE);
            holder.getLeftime().setText(msg.getDate());
            holder.getRightcontent().setVisibility(View.GONE);
            holder.getRighttime().setVisibility(View.GONE);
        } else {
            holder.getRightcontent().setVisibility(View.VISIBLE);
            holder.getRighttime().setVisibility(View.VISIBLE);
            holder.getRightcontent().setText(msg.getMessage());
            holder.getRighttime().setText(msg.getDate());
            holder.getLeftcontent().setVisibility(View.GONE);
            holder.getLeftime().setVisibility(View.GONE);

        }
    }

    class ViewHolder {
        private TextView leftime;
        private TextView righttime;

        private TextView leftcontent;
        private TextView rightcontent;
        private View mView;

        public ViewHolder(View vv) {
            this.mView = vv;
        }

        private TextView getLeftime() {
            if (leftime == null) {
                leftime = (TextView) mView.findViewById(R.id.tx_time_left);
            }
            return leftime;
        }

        private TextView getRighttime() {
            if (righttime == null) {
                righttime = (TextView) mView.findViewById(R.id.tx_time_right);
            }
            return righttime;
        }

        private TextView getLeftcontent() {
            if (leftcontent == null) {
                leftcontent = (TextView) mView.findViewById(R.id.tx_content_left);
            }
            return leftcontent;
        }

        private TextView getRightcontent() {
            if (rightcontent == null) {
                rightcontent = (TextView) mView.findViewById(R.id.tx_content_right);
            }
            return rightcontent;
        }
    }

}
